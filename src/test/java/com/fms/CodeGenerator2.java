package com.fms;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

public class CodeGenerator2 {
    public static void main(String[] args) {
        String url = "jdbc:mysql:///financial"; //定义变量，方便后续修改
        String username = "root";
        String password = "123456";
        String moduleName = "sys";
        String mapperLocation = "D:\\FinancialSys\\backend\\src\\main\\resources\\mapper\\" + moduleName;
        String tableName = "goal,note,notice";


        FastAutoGenerator.create(url, username, password) //使用定义好的变量名字
                .globalConfig(builder -> {
                    builder.author("Jinfeng") // 设置作者
                            //.enableSwagger() // 开启 swagger 模式
                            //.fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\FinancialSys\\backend\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.fms") // 设置父包名
                            .moduleName(moduleName) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, mapperLocation)); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tableName); // 设置需要生成的表名
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

}
