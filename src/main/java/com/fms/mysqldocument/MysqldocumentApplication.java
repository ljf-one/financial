package com.fms.mysqldocument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqldocumentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqldocumentApplication.class, args);
    }

}

