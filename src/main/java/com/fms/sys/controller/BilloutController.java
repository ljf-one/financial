package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.Billin;
import com.fms.sys.entity.Billout;
import com.fms.sys.service.IBilloutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
@RestController
@RequestMapping("/billout")
public class BilloutController {

    @Autowired
    private IBilloutService billoutService;

    @GetMapping("/list")
    public Result<Map<String,Object>> getBillOutList(@RequestParam(value="theme",required = false) String theme,
                                                    @RequestParam(value = "reason",required = false) String reason,
                                                    @RequestParam(value = "pageNo") Long pageNo,
                                                    @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<Billout> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(theme),Billout::getTheme,theme);
        wrapper.eq(StringUtils.hasLength(reason),Billout::getReason,reason);
//        wrapper.orderByDesc(User::getId);//用户ID降序排序

        Page<Billout> page = new Page<>(pageNo,pageSize);
        billoutService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());//返回参数，前后端统一名称
        data.put("rows",page.getRecords()); //返回查询结果集

        return  Result.success(data);
    }

    @PostMapping
    public Result<?> addBillout(@RequestBody Billout billout){
        billoutService.save(billout);
        return Result.success("成功新增支出账单");
    }

    @PutMapping
    public Result<?> updateBillout(@RequestBody Billout billout){
        billoutService.getById(billout);
        return Result.success("成功修改支出账单");
    }

    // 要根据ID获取对应的用户信息
    @GetMapping("/{id}")
    public  Result<Billout> getBilloutById(@PathVariable("id") Integer id){
        Billout billout = billoutService.getById(id);
        return  Result.success(billout);
    }

    @DeleteMapping("/{id}")
    public Result<Billout> deleteBilloutById(@PathVariable("id") Integer id){
        billoutService.removeById(id);
        return Result.success("成功删除支出记录");
    }

}
