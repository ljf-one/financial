package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.User;
import com.fms.sys.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
@RestController //前后端交互的是json数据，格式要统一，json.cn编译
@RequestMapping("/user")
//@CrossOrigin //跨域处理
public class UserController {
    @Autowired
    private IUserService userService; //装配service对象

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    @GetMapping("/all")  //请求查询所有的数据
    public Result<List<User>> getAllUser(){
        List<User> list = userService.list(); //userService内置了增删改查方法，调用即可
        return Result.success(list,"查询成功");
    }

    @PostMapping("/login")
    public Result<Map<String,Object>> login(@RequestBody User user){
        Map<String,Object> data =userService.login(user);
        if (data!=null){
        return Result.success(data);
        }
        return Result.fail(20002,"用户名或密码错误");
    }

    @GetMapping("/info")
    public Result<Map<String,Object>> getUserInfo(@RequestParam("token") String token){
        //根据token获取用户信息，redis
        Map<String,Object> data = userService.getUserInfo(token);
        if (data!=null){
            return Result.success(data);
        }
        return Result.fail(20003,"用户信息获取失败");
    }

    @PostMapping("/logout")
    public Result<?> logout(@RequestHeader("X-Token") String token){
        userService.logout(token);
        return Result.success("注销成功");
    }

    @GetMapping ("/list")
    public Result<Map<String,Object>> getUserList(@RequestParam (value="username",required = false) String username,
                                              @RequestParam(value = "phone",required = false) String phone,
                                              @RequestParam(value = "pageNo") Long pageNo,
                                              @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(username),User::getUsername,username);
        wrapper.eq(StringUtils.hasLength(phone),User::getPhone,phone);
        wrapper.orderByDesc(User::getId);//用户ID降序排序

        Page<User> page = new Page<>(pageNo,pageSize);
        userService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());//返回参数，前后端统一名称
        data.put("rows",page.getRecords()); //返回查询结果集

        return  Result.success(data);
    }

    @PostMapping
    public Result<?> addUser(@RequestBody User user){ //RequestBody注解是将json数据转化成user对象
//    密码加密处理，用BCryptPasswordEncoder，涉及登录逻辑改动：1、密码加密存储到db;2、登录时db和传入的比较
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(user);
        return Result.success("新增用户成功");
    }

//    修改用户
    @PutMapping
    public Result<?> updateUser(@RequestBody User user){
        user.setPassword(null);
        userService.updateById(user);
        return Result.success("修改用户成功");
    }
//    要根据ID获取对应的用户信息
    @GetMapping("/{id}")
    public  Result<User> getUserById(@PathVariable("id") Integer id){
        User user = userService.getById(id);
        return  Result.success(user);
    }

//    删除用户
    @DeleteMapping("/{id}")
    public  Result<User> deleteUserById(@PathVariable("id") Integer id){
        userService.removeById(id);
        return  Result.success("删除用户成功");
    }

//    修改密码
//    @PutMapping("/update")
//    public Result<?> updatePassword(@RequestBody User user){
//        userService.updateById(user);
//        return Result.success("修改密码成功");
//    }

//  修改个人信息
//    @PutMapping
//    public Result<?> updateInfo(@RequestBody User user){
////        user.setPassword(null);
//        userService.updateById(user);
//        return Result.success("成功修改个人信息");
//    }

}
