package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.Card;
import com.fms.sys.entity.Goal;
import com.fms.sys.entity.User;
import com.fms.sys.service.IGoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@RestController
@RequestMapping("/goal")
public class GoalController {

    @Autowired
    private IGoalService goalService;

    @GetMapping("/list")
    public Result<Map<String,Object>> getGoalList(@RequestParam(value="theme",required = false) String theme,
                                                  @RequestParam(value = "pageNo") Long pageNo,
                                                  @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<Goal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(theme),Goal::getTheme,theme);
        Page<Goal> page = new Page<>(pageNo,pageSize);
        goalService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());//返回参数，前后端统一名称
        data.put("rows",page.getRecords()); //返回查询结果集

        return  Result.success(data);
    }

    @PostMapping
    public Result<?> addGoal(@RequestBody Goal goal){ //RequestBody注解是将json数据转化成user对象
        goalService.save(goal);
        return Result.success("新增心愿成功");
    }

    //    修改用户
    @PutMapping
    public Result<?> updateGoal(@RequestBody Goal goal){
        goalService.updateById(goal);
        return Result.success("修改心愿成功");
    }
    //    要根据ID获取对应的用户信息
    @GetMapping("/{id}")
    public  Result<Goal> getGoalById(@PathVariable("id") Integer id){
        Goal goal = goalService.getById(id);
        return  Result.success(goal);
    }

    //    删除用户
    @DeleteMapping("/{id}")
    public  Result<Goal> deleteGoalById(@PathVariable("id") Integer id){
        goalService.removeById(id);
        return  Result.success("删除心愿成功");
    }

}
