package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.Card;
import com.fms.sys.entity.Goal;
import com.fms.sys.entity.Note;
import com.fms.sys.service.INoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@RestController
@RequestMapping("/note")
public class NoteController {

    @Autowired
    private INoteService noteService;

    @GetMapping("/list")
    public Result<Map<String,Object>> getNoteList(@RequestParam(value = "theme",required = false) String theme,
                                                  @RequestParam(value = "pageNo") Long pageNo,
                                                  @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<Note> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(theme),Note::getTheme,theme);

        Page<Note> page = new Page<>(pageNo,pageSize);
        noteService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());//返回参数，前后端统一名称
        data.put("rows",page.getRecords()); //返回查询结果集

        return  Result.success(data);
    }

    @PostMapping
    public Result<?> addNote(@RequestBody Note note){ //RequestBody注解是将json数据转化成user对象
        noteService.save(note);
        return Result.success("新增心备忘录成功");
    }

    @PutMapping
    public Result<?> updateNote(@RequestBody Note note){
        noteService.updateById(note);
        return Result.success("修改备忘录成功");
    }

    @GetMapping("/{id}")
    public  Result<Note> getNoteById(@PathVariable("id") Integer id){
        Note note = noteService.getById(id);
        return  Result.success(note);
    }

    @DeleteMapping("/{id}")
    public  Result<Note> deleteNoteById(@PathVariable("id") Integer id){
        noteService.removeById(id);
        return  Result.success("成功删除备忘录");
    }
}
