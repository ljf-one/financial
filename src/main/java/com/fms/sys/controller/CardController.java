package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.Billin;
import com.fms.sys.entity.Card;
import com.fms.sys.entity.Note;
import com.fms.sys.service.ICardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@RestController
@RequestMapping("/card")
public class CardController {

    @Autowired
    private ICardService cardService;

    @GetMapping("/list")
    public Result<Map<String,Object>> getCardList(@RequestParam(value="cardNo",required = false) String cardNo,
                                                    @RequestParam(value = "pageNo") Long pageNo,
                                                    @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<Card> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(cardNo),Card::getCardNo,cardNo);

        Page<Card> page = new Page<>(pageNo,pageSize);
        cardService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());//返回参数，前后端统一名称
        data.put("rows",page.getRecords()); //返回查询结果集

        return  Result.success(data);
    }

    @PostMapping
    public Result<?> addCard(@RequestBody Card card){ //RequestBody注解是将json数据转化成user对象
        cardService.save(card);
        return Result.success("成功新增银行卡");
    }

    @PutMapping
    public Result<?> updateCard(@RequestBody Card card){
        cardService.updateById(card);
        return Result.success("成功修改银行卡信息");
    }

    @GetMapping("/{id}")
    public  Result<Card> getCardById(@PathVariable("id") Integer id){
        Card card = cardService.getById(id);
        return  Result.success(card);
    }

    @DeleteMapping("/{id}")
    public  Result<Card> deleteCardById(@PathVariable("id") Integer id){
        cardService.removeById(id);
        return  Result.success("成功删除该张银行卡");
    }

}
