package com.fms.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fms.common.vo.Result;
import com.fms.sys.entity.Goal;
import com.fms.sys.entity.Notice;
import com.fms.sys.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@RestController
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    private INoticeService noticeService;

    @GetMapping("/list")
    public Result<Map<String,Object>> getNoticeList(@RequestParam(value = "theme" ,required = false) String theme,
                                                    @RequestParam(value = "pageNo") Long pageNo,
                                                    @RequestParam(value = "pageSize") Long pageSize){
        LambdaQueryWrapper<Notice> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(theme),Notice::getTheme,theme);

        Page<Notice> page = new Page<>(pageNo,pageSize);
        noticeService.page(page,wrapper);

        Map<String,Object> data = new HashMap<>();
        data.put("total",page.getTotal());
        data.put("rows",page.getRecords());

        return Result.success(data);
    }

    @PostMapping
    public Result<?> addNotice(@RequestBody Notice notice){ //RequestBody注解是将json数据转化成user对象
        noticeService.save(notice);
        return Result.success("新增公告成功");
    }

    @PutMapping
    public Result<?> updateNotice(@RequestBody Notice notice){
        noticeService.updateById(notice);
        return Result.success("修改公告成功");
    }

    @GetMapping("/{id}")
    public  Result<Notice> getNoticeById(@PathVariable("id") Integer id){
        Notice notice = noticeService.getById(id);
        return Result.success(notice);
    }

    @DeleteMapping("/{id}")
    public  Result<Notice> deleteNoticeById(@PathVariable("id") Integer id){
        noticeService.removeById(id);
        return  Result.success("删除公告成功");
    }

}
