package com.fms.sys.service;

import com.fms.sys.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
public interface IRoleService extends IService<Role> {

}
