package com.fms.sys.service.impl;

import com.fms.sys.entity.Notice;
import com.fms.sys.mapper.NoticeMapper;
import com.fms.sys.service.INoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

}
