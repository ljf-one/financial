package com.fms.sys.service.impl;

import com.fms.sys.entity.Goal;
import com.fms.sys.mapper.GoalMapper;
import com.fms.sys.service.IGoalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@Service
public class GoalServiceImpl extends ServiceImpl<GoalMapper, Goal> implements IGoalService {

}
