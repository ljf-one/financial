package com.fms.sys.service.impl;

import com.fms.sys.entity.Note;
import com.fms.sys.mapper.NoteMapper;
import com.fms.sys.service.INoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
@Service
public class NoteServiceImpl extends ServiceImpl<NoteMapper, Note> implements INoteService {

}
