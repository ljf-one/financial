package com.fms.sys.service.impl;

import com.fms.sys.entity.Billin;
import com.fms.sys.mapper.BillinMapper;
import com.fms.sys.service.IBillinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
@Service
public class BillinServiceImpl extends ServiceImpl<BillinMapper, Billin> implements IBillinService {

}
