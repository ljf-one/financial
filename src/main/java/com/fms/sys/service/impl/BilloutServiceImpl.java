package com.fms.sys.service.impl;

import com.fms.sys.entity.Billout;
import com.fms.sys.mapper.BilloutMapper;
import com.fms.sys.service.IBilloutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
@Service
public class BilloutServiceImpl extends ServiceImpl<BilloutMapper, Billout> implements IBilloutService {

}
