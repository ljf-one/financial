package com.fms.sys.service;

import com.fms.sys.entity.Note;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
public interface INoteService extends IService<Note> {

}
