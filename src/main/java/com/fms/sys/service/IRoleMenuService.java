package com.fms.sys.service;

import com.fms.sys.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
