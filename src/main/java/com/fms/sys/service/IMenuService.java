package com.fms.sys.service;

import com.fms.sys.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
public interface IMenuService extends IService<Menu> {

}
