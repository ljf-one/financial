package com.fms.sys.service;

import com.fms.sys.entity.Billout;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
public interface IBilloutService extends IService<Billout> {

}
