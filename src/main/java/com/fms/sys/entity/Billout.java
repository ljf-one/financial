package com.fms.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
public class Billout implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 支出主题
     */
    private String theme;

    /**
     * 支出金额
     */
    private BigDecimal amount;

    /**
     * 支出时间
     */
    private LocalDateTime consumeDate;

    /**
     * 支出类型
     */
    private String consumeType;

    /**
     * 支出备注
     */
    private String reason;

    /**
     * 删除标识
     */
    private Integer deleteFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public LocalDateTime getConsumeDate() {
        return consumeDate;
    }

    public void setConsumeDate(LocalDateTime consumeDate) {
        this.consumeDate = consumeDate;
    }
    public String getConsumeType() {
        return consumeType;
    }

    public void setConsumeType(String consumeType) {
        this.consumeType = consumeType;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @Override
    public String toString() {
        return "Billout{" +
            "id=" + id +
            ", theme=" + theme +
            ", amount=" + amount +
            ", consumeDate=" + consumeDate +
            ", consumeType=" + consumeType +
            ", reason=" + reason +
            ", deleteFlag=" + deleteFlag +
        "}";
    }
}
