package com.fms.sys.mapper;

import com.fms.sys.entity.Billout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-17
 */
public interface BilloutMapper extends BaseMapper<Billout> {

}
