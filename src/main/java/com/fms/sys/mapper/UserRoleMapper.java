package com.fms.sys.mapper;

import com.fms.sys.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
