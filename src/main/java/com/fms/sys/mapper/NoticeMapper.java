package com.fms.sys.mapper;

import com.fms.sys.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Jinfeng
 * @since 2023-03-18
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
