package com.fms.sys.mapper;

import com.fms.sys.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Jinfeng
 * @since 2023-02-24
 */
public interface RoleMapper extends BaseMapper<Role> {

}
