package com.fms.common.vo;

//该公共响应类作用是前后端对接格式统一，可通过json.cn编译

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data   //该注解生成get、set方法
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer code;
    private String message;
    private T data; //范型T，类上面加定义

    //返回数据做封装，避免冗余，设置以下静态方法

    public static<T> Result<T> success(){
        return new Result<>(20000,"success",null);
    }

    //其它指定的message、data,重载
    public static<T> Result<T> success(T data){
        return new Result<>(20000,"success",data);//传入数据
    }

    public static<T> Result<T> success(T data,String message){
        return new Result<>(20000,message,data);//不但传数据，还带message
    }

    public static<T> Result<T> success(String message){
        return new Result<>(20000,message,null);//不传数据，带message
    }

    public static<T>  Result<T> fail(){
        return new Result<>(20001,"fail",null);
    }

    public static<T>  Result<T> fail(Integer code){
        return new Result<>(code,"fail",null);
    }

    public static<T>  Result<T> fail(Integer code, String message){
        return new Result<>(code,message,null);
    }

    public static<T>  Result<T> fail( String message){
        return new Result<>(20001,message,null);
    }
}
